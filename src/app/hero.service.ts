import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Address } from './address';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class HeroService {

  private addressUrl = 'api/add_res';  // URL to web api

  constructor(
    private http: HttpClient) { }

  getHeroes (): Observable<Address[]> {
    return this.http.get<Address[]>(this.addressUrl)
      .pipe(
        catchError(this.handleError('getHeroes', []))
      );
  }

  getHeroNo404<Data>(id: number): Observable<Address> {
    const url = `${this.addressUrl}/?id=${id}`;
    return this.http.get<Address[]>(url)
      .pipe(
        map(heroes => heroes[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
        }),
        catchError(this.handleError<Address>(`getHero id=${id}`))
      );
  }

  getHero(id: number): Observable<Address> {
    const url = `${this.addressUrl}/${id}`;
    return this.http.get<Address>(url).pipe(
      catchError(this.handleError<Address>(`getHero id=${id}`))
    );
  }

  searchHeroes(term: string): Observable<Address[]> {
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Address[]>(`${this.addressUrl}/?name=${term}`).pipe(
      catchError(this.handleError<Address[]>('searchHeroes', []))
    );
  }


  addHero (address: Address): Observable<Address> {
    return this.http.post<Address>(this.addressUrl, address, httpOptions).pipe(
      catchError(this.handleError<Address>('addHero'))
    );
  }

  deleteHero (address: Address | number): Observable<Address> {
    const id = typeof address === 'number' ? address : address.id;
    const url = `${this.addressUrl}/${id}`;

    return this.http.delete<Address>(url, httpOptions).pipe(
      catchError(this.handleError<Address>('deleteHero'))
    );
  }

  updateHero (address: Address): Observable<any> {
    return this.http.put(this.addressUrl, address, httpOptions).pipe(
      catchError(this.handleError<any>('updateHero'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation 
   * @param result 
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

}
